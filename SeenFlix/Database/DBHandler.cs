using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Android.Util;
using SeenFlix.Models;
using System.IO;

namespace SeenFlix
{
    public static class DBHandler
    {
        private const string TAG = "DBHandler";
        public static string DEFAULT_PATH = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "seen.db");
        public static string WATCHLIST_PATH = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "watchlist.db");

        public static void CreateDb(String path)
        {
            try
            {
                var con = new SQLiteConnection(path);
                con.CreateTable<Movie>();

            }
            catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
            }
            Log.Info(TAG, "Database created at " + path);
        }

        public static void DeleteDb(String path)
        {
            try
            {
                var con = new SQLiteConnection(path);
                con.DropTable<Movie>();
            }
            catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
            }
            Log.Info(TAG, "Database deleted from " + path);
        }

        public static void insertUpdateMovie(Movie movie, String path)
        {
            
            try { 
                var db = new SQLiteConnection(path);
                var temp = db.Table<Movie>().Where<Movie>(c => c.Equals(movie)).Count();
                if (temp == 0){
                   if(db.Insert(movie) > 0)
                    {
                        Log.Info(TAG, movie.Title + " succesfully added to db.");
                    }
                    else
                    {
                        Log.Error(TAG, "Something fucked up");
                    }
                }else
                    Log.Info(TAG, "db already contains this movie: " + movie.Title);

            }
            catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
            }           
        }

        public static Movie GetMovie(int id, String path)
        {
            try
            {
                var db = new SQLiteConnection(path);
                var movie = db.Get<Movie>(id);
                Log.Info(TAG, "Movie " + movie.Title + " retrieved");
                return movie;

            }
            catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
            }
            return null;

        }

        public static Movie GetByImdb(string ImdbId,string path)
        {
            try
            {
                var db = new SQLiteConnection(path);
                var movieQr = db.Table<Movie>().Where( m => (m.ImdbId == ImdbId));
                if (movieQr.Count() == 0)
                    return null;
                var movie = movieQr.First();
                Log.Info(TAG, "Movie " + movie.Title + " retrieved");
                return movie;               
            }
            catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
            }
            return null;
        }

        public static void Delete(Movie movie, String path)
        {
            try
            {
                var db = new SQLiteConnection(path);
                db.Delete<Movie>(movie.ID);
                Log.Info(TAG, "Movie " + movie.Title + "Deleted");

            }
            catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
            }

        }

        public static void DeleteAll(String path)
        {
            try
            {
                var db = new SQLiteConnection(path);
                db.DeleteAll<Movie>();
                Log.Info(TAG,"Database flushed");

            }catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
            }
        }

        public static Movie[] getMovies(string path)
        {
            List<Movie> movies;
            try
            {
                var con = new SQLiteConnection(path);
                movies = con.Table<Movie>().ToList();
                Log.Info(TAG, "Movies retrieved.");
                return movies.ToArray();
            }
            catch (SQLiteException ex)
            {
                Log.Error(TAG, ex.Message);
                return null;
            }
        }
    }
}