using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using OmdbWrapper.Models;

namespace SeenFlix.Models
{
    class SearchMovie : OSearchMovie
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public static SearchMovie Convert(OSearchMovie os)
        {
            var searchMovie = new SearchMovie();
            searchMovie.ImdbId = os.ImdbId;
            searchMovie.Title = os.Title;
            searchMovie.Type = os.Type;
            searchMovie.Year = os.Year;
            return searchMovie;
        }

        public override string ToString()
        {
            return (Title + " (" + Year + ")" + ", " + Type);
        }
    }
}