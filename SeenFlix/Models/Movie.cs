using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using OmdbWrapper.Models;

namespace SeenFlix.Models
{
	public class Movie : OMovie, IComparable
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public static Movie Convert(OMovie m)
        {
            Movie newMovie = new Movie();
            newMovie.Actors = m.Actors;
            newMovie.Awards = m.Awards;
            newMovie.BoxOffice = m.BoxOffice;
            newMovie.Country = m.Country;
            newMovie.Director = m.Director;
            newMovie.DVD = m.DVD;
            newMovie.Genre = m.Genre;
            newMovie.ImdbId = m.ImdbId;
            newMovie.ImdbRating = m.ImdbRating;
            newMovie.ImdbVotes = m.ImdbVotes;
            newMovie.Language = m.Language;
            newMovie.Metascore = m.Metascore;
            newMovie.Plot = m.Plot;
            newMovie.Poster = m.Poster;
            newMovie.Production = m.Production;
            newMovie.Rated = m.Rated;
            newMovie.Released = m.Released;
            newMovie.Response = m.Response;
            newMovie.Runtime = m.Runtime;
            newMovie.Title = m.Title;
            newMovie.TomatoConsensus = m.TomatoConsensus;
            newMovie.TomatoFresh = m.TomatoFresh;
            newMovie.TomatoImage = m.TomatoImage;
            newMovie.TomatoMeter = m.TomatoMeter;
            newMovie.TomatoRating = m.TomatoRating;
            newMovie.TomatoReviews = m.TomatoReviews;
            newMovie.TomatoRotten = m.TomatoRotten;
            newMovie.TomatoUserMeter = m.TomatoUserMeter;
            newMovie.TomatoUserRating = m.TomatoUserRating;
            newMovie.TomatoUserReviews = m.TomatoUserReviews;
            newMovie.Type = m.Type;
            newMovie.Website = m.Website;
            newMovie.Writer = m.Writer;
            newMovie.Year = m.Year;
            return newMovie;
        }

        public override string ToString()
        {
            return (Title + " (" + Year + ")");
        }

        public override bool Equals(object obj)
        {
           if(obj.GetType().Equals(this.GetType()))
            {
                var movObj = (Movie)obj;
                return (movObj.Title == this.Title)
                    && (movObj.Director == this.Director)
                    && (movObj.Year == this.Year);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Title.GetHashCode() * Director.GetHashCode() * Year.GetHashCode() + 42;
        }

        public int CompareTo(object obj)
        {
            var mov = (Movie)obj;
            return String.Compare(Title, mov.Title);
        }
    }
}