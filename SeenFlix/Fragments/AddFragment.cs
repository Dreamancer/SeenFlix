using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace SeenFlix.Fragments
{
    public class AddFragment : DialogFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            var view = inflater.Inflate(Resource.Layout.AddFragment, container, false);
            

            view.FindViewById<Button>(Resource.Id.addListButton).Click += delegate
            {
                var newName = view.FindViewById<EditText>(Resource.Id.addListText).Text;
                MainActivity callingActivity = (MainActivity)Activity;
                callingActivity.onAddList(newName);
                Dismiss();
            };
            Dialog.SetTitle("Add new list");
            return view;
        }
    }
}