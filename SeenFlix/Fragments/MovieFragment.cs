﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SeenFlix.Adapters;
using SeenFlix.Fragments;
using SeenFlix.Models;
using System.Collections.Generic;

namespace SeenFlix
{
    class MovieFragment : ListFragment
    {
        private string currentListPath = null;
        private View selected = null;

        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);
            currentListPath = Arguments.GetString("list");
            if (currentListPath.Equals("search"))
                ListAdapter = new MovieAdapter(this.Activity, new List<Models.Movie>());
             else {
                var list = new List<Models.Movie>(DBHandler.getMovies(currentListPath));
                list.Sort();
                ListAdapter = new MovieAdapter(this.Activity, list);
            }

            ((MainActivity)Activity).Adapter = ListAdapter as MovieAdapter;
        }

        public override void OnListItemClick(ListView l, View v, int position, long id)
        {
            selected = v;
            v.Selected = true;
            var movie = (ListAdapter as ArrayAdapter<Models.Movie>).GetItem(position);
            MainActivity activity = Activity as MainActivity;

            var movieActivity = new Intent(this.Activity, typeof(MovieActivity));              
            if (currentListPath.Equals("search")) { 
                movieActivity.PutExtra("imdb_id", movie.ImdbId);
                SearchActivity sActivity = activity as SearchActivity;
                string add = sActivity.Intent.GetStringExtra("adding");
                string path = sActivity.Intent.GetStringExtra("path");
                if (add != null && path != null) { 
                    movieActivity.PutExtra("adding", add);
                    movieActivity.PutExtra("path", path);
                }
                
            }
            else{ 
                movieActivity.PutExtra("id", movie.ID);
                movieActivity.PutExtra("listName", activity.FragmentName);
                movieActivity.PutExtra("listPath", currentListPath);
            //    Activity.StartActivityForResult(movieActivity, 0);
            }
            string[] DbListNames = new string[activity.DbLists.Count];
            string[] DbListPaths = new string[activity.DbLists.Count];
            activity.DbLists.Keys.CopyTo(DbListNames,0);
            activity.DbLists.Values.CopyTo(DbListPaths, 0);
            movieActivity.PutExtra("DbListNames", DbListNames);
            movieActivity.PutExtra("DbListPaths", DbListPaths);
            Activity.StartActivityForResult(movieActivity, 0);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            ListView.Divider = new ColorDrawable(Color.Rgb(51, 51, 51));
            ListView.DividerHeight = 1;

        }

      /*  public override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            selected.Selected = false;
        }*/
    }
       
}
