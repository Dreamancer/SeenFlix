﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using SeenFlix.Models;

namespace SeenFlix
{
	public class SettingsFragment : Fragment
	{
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			var view = inflater.Inflate(Resource.Layout.SettingsFragment, container, false);

			TextView count = view.FindViewById<TextView>(Resource.Id.listCountText);
			count.Text = DBHandler.getMovies (DBHandler.DEFAULT_PATH).Count().ToString();

			Button clearButton = view.FindViewById<Button>(Resource.Id.clearButton);
			clearButton.Click += delegate(object sender, EventArgs e) {

				DBHandler.DeleteAll(DBHandler.DEFAULT_PATH);
                count.Text = DBHandler.getMovies(DBHandler.DEFAULT_PATH).Count().ToString();
            };

            return view;	
		}
	}
}

