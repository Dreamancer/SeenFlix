using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Java.Lang;
using System.Collections;
using System.Threading.Tasks;
using Android.Graphics.Drawables;
using Android.Support.V4.Content.Res;
using Android.Content.Res;

namespace SeenFlix.Adapters
{
    class MovieAdapter : ArrayAdapter<Models.Movie>
    {
        private Activity context;
        private Dictionary<int,Bitmap> imgs = new Dictionary<int, Bitmap>();
        public override Filter Filter { get; }
        public List<Models.Movie> Movies;

        
        public MovieAdapter(Activity context, List<Models.Movie> movies) : base(context,Resource.Layout.MovieItem,movies) {
            this.context = context;
            this.Movies = movies;
            for (int i = 0; i < Count; i++)
            {
                Bitmap poster = MovieActivity.GetImageBitmapFromUrl(GetItem(i).Poster);
                if (poster == null)
                {
                    BitmapDrawable temp = ResourcesCompat.GetDrawable(context.Resources, Resource.Drawable.not_available, null) as BitmapDrawable;
                    poster = temp.Bitmap;
                }
                imgs.Add(i, poster);               
            }
            Filter = new MovieFilter(this);
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            MovieViewHolder holder = null;             
            View view = convertView;
            if (view != null) // otherwise create a new one
                holder = view.Tag as MovieViewHolder;//context.LayoutInflater.Inflate(Resource.Layout.MovieItem, null);

            if (holder == null)
            {
                holder = new MovieViewHolder();
                view = context.LayoutInflater.Inflate(Resource.Layout.MovieItem, null);
                holder.Name = view.FindViewById<TextView>(Resource.Id.searchMovie_title);
                holder.Description = view.FindViewById<TextView>(Resource.Id.searchMovie_year_genre);
                holder.Image = view.FindViewById<ImageView>(Resource.Id.movieIcon);
                view.Tag = holder;
            }
            var movie = GetItem(position);
            holder.Name.Text = movie.Title + " (" + movie.Year + ")";
            holder.Description.Text = movie.Genre + ", " + movie.Type;

            holder.Image.SetImageBitmap(imgs[position]);

            return view;
        }

        public override void AddAll(ICollection collection)
        {
            base.AddAll(collection);
            imgs.Clear();
            var movies = (ICollection<Models.Movie>) collection;

            for (int i = Count-1; i >= Count - collection.Count ; i--)
            {

                Bitmap poster = null;
                if (movies.ElementAt(i).Poster != "N/A")
                     poster = MovieActivity.GetImageBitmapFromUrl(movies.ElementAt(i).Poster);
                if (poster == null)
                     poster = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.not_available);
                imgs.Add(i, poster);
            }
        }

        

        private class MovieFilter : Filter
        {
            private MovieAdapter _adapter;

            public MovieFilter(MovieAdapter adapter)
            {
                _adapter = adapter;
            }

      

            protected override FilterResults PerformFiltering(ICharSequence constraint)
            {
                var result = new FilterResults();                            

                if (constraint == null)
                    return result;

                var original = _adapter.Movies;
                var filtered = new List<Models.Movie>();

                if (original != null)
                {
                    filtered.AddRange(
                        original.Where(
                            o => o.Title.ToLower().Contains(constraint.ToString())));
                }
                result.Values = FromArray(filtered.Select(r => r.ToJavaObject()).ToArray());
                result.Count = filtered.Count;

                constraint.Dispose();
                return result;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results)
            {
                _adapter.Clear();
                using (var values = results.Values)
                    _adapter.AddAll(values.ToArray<Java.Lang.Object>()
                        .Select(r => r.ToNetObject<Models.Movie>()).ToList());

                _adapter.NotifyDataSetChanged();

                // Don't do this and see GREF counts rising
                constraint.Dispose();
                results.Dispose();
            }
        }
    }
}