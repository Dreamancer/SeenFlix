﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Support.V4.View;
using Android.Runtime;
using SeenFlix.Models;
using OmdbWrapper.Models;
using System.Collections.Generic;
using System.Collections;
using Android.Views.InputMethods;
using Android.Content;
using Android.Views.Animations;
using SeenFlix.Fragments;
using SeenFlix.Adapters;
using System.IO;

namespace SeenFlix
{
    [Activity(Label = "SeenFlix", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        protected DrawerLayout drawerLayout;
        protected Android.Support.V7.Widget.SearchView _searchView;
        protected Fragment fragment;
        protected string fragmentName;
        public string FragmentName { get { return fragmentName; }}
        protected ArrayAdapter _adapter;
        public ArrayAdapter Adapter { get { return _adapter; } set { _adapter = value; } }
        protected LinearLayout linearProgress;
        protected string DbListPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Lists.mvs");
        public Dictionary<string,string> DbLists = new Dictionary<string,string>();
        protected FloatingActionButton fab;

        protected override void OnCreate(Bundle savedInstanceState)       
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            //Create file containing paths to default movie lists (if it doesn't already exist)
            if (!File.Exists(DbListPath))
                using (StreamWriter w = File.CreateText(DbListPath))
                {
                    w.WriteLine(Resources.GetString(Resource.String.navSeen) + ";"+DBHandler.DEFAULT_PATH);
                    w.WriteLine(Resources.GetString(Resource.String.navWatchlist) + ";" + DBHandler.WATCHLIST_PATH);
                }
                // Initialize DB
                LoadDB();
            // Initialize toolbar and navigation
            InitToolbarAndNav();
            //Load default screen
            string nameFromResult;
            if ((nameFromResult = Intent.GetStringExtra("fragment")) != null) { 
              //  if (DbLists.ContainsKey(nameFromResult))
                    SetFragment(nameFromResult, DbLists[nameFromResult]);
           }
            else
                    SetFragment(Resources.GetString(Resource.String.navSeen), DBHandler.DEFAULT_PATH);

			linearProgress = (LinearLayout) FindViewById(Resource.Id.linearProgress);
        }

        protected virtual void InitToolbarAndNav()
        {
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            fab = FindViewById<FloatingActionButton>(Resource.Id.addFab);
            SetSupportActionBar(toolbar);
        //    SupportActionBar.SetTitle(Resource.String.seenAppTitle);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);
            // Attach item selected handler to navigation view
            var navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;
            // Create ActionBarDrawerToggle button and add it to the toolbar
            var drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, Resource.String.openDrawer, Resource.String.closeDrawer);
            drawerLayout.SetDrawerListener(drawerToggle);
            drawerToggle.SyncState();

            PopulateMenu();
        }

        protected virtual void LoadDB()
        {
            using(StreamReader r = File.OpenText(DbListPath))
            {
                String line;
                int i=0;
                while ((line = r.ReadLine()) != null) {
                    var parsedLine = line.Split(';');
                    DbLists.Add(parsedLine[0],parsedLine[1]);
                    DBHandler.CreateDb(parsedLine[1]);
                    
                }
            }
        }

        protected virtual void SetFragment(string name, string path)
        {
            if (DBHandler.getMovies(path).Length == 0)
                fragment = new EmptyFragment();
            else { 
                fragment = new MovieFragment();
                var movFrag = fragment as MovieFragment;
              //  _adapter = movFrag.ListAdapter as MovieAdapter;
            }

            Bundle b = new Bundle();
                b.PutString("list", path);         
                fragment.Arguments = b;
                var ft = FragmentManager.BeginTransaction();
                ft.Replace(Resource.Id.HomeFrameLayout, fragment, name);
                ft.AddToBackStack(null);
                ft.Commit();
                FragmentManager.ExecutePendingTransactions();

            SupportActionBar.Title = "Seenflix - " + name;
            if(_searchView != null)
            {
                var search = FindViewById(Resource.Id.search);
                search.Visibility = ViewStates.Visible;
                _searchView.QueryHint = "Filter " + name;
            }
            fragmentName = name;

            fab.Show();
 
        }

        protected void FilterSearchView (object s, Android.Support.V7.Widget.SearchView.QueryTextChangeEventArgs e)
        {
            if (_adapter != null)
                _adapter.Filter.InvokeFilter(e.NewText);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {  
            MenuInflater.Inflate(Resource.Menu.toolbarmenu, menu);

            var item = menu.FindItem(Resource.Id.search);
            if(fragmentName != null)
            item.SetIcon(Resource.Drawable.ic_filter_list_white_24dp);
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            _searchView.QueryHint = "Filter " + fragmentName;
          
                _searchView.QueryTextChange += FilterSearchView;          

            _searchView.FocusChange += delegate
            {
                var inm = (InputMethodManager)GetSystemService(InputMethodService);
                inm.HideSoftInputFromWindow(CurrentFocus.WindowToken, HideSoftInputFlags.None);
            };

            _searchView.QueryTextSubmit += (s, e) =>
            {           
                _searchView.ClearFocus();

                e.Handled = true;               
            };

            fab.Click += delegate
            {
                Intent intent = new Intent(this, typeof(SearchActivity));
                intent.PutExtra("adding", fragmentName);
                intent.PutExtra("path", DbLists[fragmentName]);
                StartActivityForResult(intent,0);
                OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
            };
            return true;
        }

        protected virtual void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var itemTitle = e.MenuItem.TitleFormatted.ToString();
            if (itemTitle == fragmentName)
            {
                drawerLayout.CloseDrawers();
            }else
                if(DbLists.ContainsKey(itemTitle))
                {
                    SetFragment(itemTitle, DbLists[itemTitle]);              
                    drawerLayout.CloseDrawers();
                }
                else
                    switch (e.MenuItem.ItemId)
                    {
                        case (Resource.Id.nav_netSearch):
                            drawerLayout.DrawerClosed += delegate
                            {
                                Intent resultIntent = new Intent(this, typeof(SearchActivity));
                                Finish();
                             //   OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
                                StartActivity(resultIntent);
                                OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);                   
                                // OverridePendingTransition(Android.Resource.Animation.FadeIn, 0);
                            };
                            drawerLayout.CloseDrawers();
                        break;
			            case (Resource.Id.nav_settings):
				                var ft = FragmentManager.BeginTransaction ();
				                fragment = new SettingsFragment ();
                                ft.Replace(Resource.Id.HomeFrameLayout, fragment,"SETTINGS");
                                ft.Commit();
                                FragmentManager.ExecutePendingTransactions();
                                SupportActionBar.Title = "Seenflix - Settings";
                                fragmentName = itemTitle;
                                fab.Hide();
                                var search = FindViewById(Resource.Id.search);
                                search.Visibility = ViewStates.Invisible;
                                drawerLayout.CloseDrawers();
                        break;
                        case (Resource.Id.nav_add):
                            var transaction = FragmentManager.BeginTransaction();
                            var dialog = new AddFragment();
                            dialog.Show(transaction, "AddFragment");
                        break;

                    }
            // Close drawer
          //  e.MenuItem.SetChecked(true);   
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            SetFragment(fragmentName, DbLists[fragmentName]);
            base.OnActivityResult(requestCode, resultCode, data);          
        }

        public void deleteList(string name)
        {
            var navView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navView.
        }

        public void onAddList(string name)
        {
            if (!DbLists.ContainsKey(name)) { 
                var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), name + ".db");
                DBHandler.CreateDb(path);
                Toast.MakeText(this, "List " + name + " created",ToastLength.Short).Show();

                var navView = FindViewById<NavigationView>(Resource.Id.nav_view);
                var menu = navView.Menu;
             //   var subMenu = menu.FindItem(Resource.Id.nav_subLists).SubMenu;
              //  subMenu.Add(name);
                menu.Add(Resource.Id.movieGroup, Menu.None, 0, name);

                var count = navView.ChildCount;
                for (int i = 0; i < count; i++)
                {
                    View child = navView.GetChildAt(i);
                    if (child != null && child is ListView) {
                         ListView menuView = (ListView)child;
                         HeaderViewListAdapter adapter = (HeaderViewListAdapter)menuView.Adapter;
                         BaseAdapter wrapped = (BaseAdapter)adapter.WrappedAdapter;
                         wrapped.NotifyDataSetChanged();
                    }
                }
                if (File.Exists(DbListPath))
                    using (StreamWriter w = File.AppendText(DbListPath))
                    {
                        w.WriteLine(name + ";" + path);
                    }
                DbLists.Add(name, path);
            }else
                Toast.MakeText(this, "List " + name + " already exists!", ToastLength.Short).Show();
            drawerLayout.CloseDrawers();

        }

        protected void PopulateMenu()
        {
            var navView = FindViewById<NavigationView>(Resource.Id.nav_view);
            var menu = navView.Menu;
          //  var subMenu = menu.FindItem(Resource.Id.nav_subLists).SubMenu;
            foreach (string name in DbLists.Keys) {
                if (!name.Equals(Resources.GetString(Resource.String.navSeen)) && !name.Equals(Resources.GetString(Resource.String.navWatchlist))) {
                    menu.Add(Resource.Id.movieGroup, Menu.None, 0, name);
                }
            }

            var count = navView.ChildCount;
            for (int i = 0; i < count; i++)
            {
                View child = navView.GetChildAt(i);
                if (child != null && child is ListView)
                {
                    ListView menuView = (ListView)child;
                    HeaderViewListAdapter adapter = (HeaderViewListAdapter)menuView.Adapter;
                    BaseAdapter wrapped = (BaseAdapter)adapter.WrappedAdapter;
                    wrapped.NotifyDataSetChanged();
                }
            }
        }
    }
}

