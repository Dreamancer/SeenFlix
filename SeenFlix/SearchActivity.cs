using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SeenFlix.Models;
using OmdbWrapper.Models;
using Android.Views.InputMethods;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Graphics;
using System.Threading.Tasks;
using SeenFlix.Adapters;
using SeenFlix.Fragments;

namespace SeenFlix
{
    [Activity(Label = "SeenFlix - Search online", Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    class SearchActivity : MainActivity
    {     

        protected override void InitToolbarAndNav()
        {
            base.InitToolbarAndNav();          
            fab.Visibility = ViewStates.Gone;
        }

        protected override void SetFragment(string name,string path)
        {

            Bundle b = new Bundle();
            b.PutString("list", "search");
            fragment = new MovieFragment();
            fragment.Arguments = b;
            var ft = FragmentManager.BeginTransaction();
            ft.Replace(Resource.Id.HomeFrameLayout, fragment, "SEARCHONLINE");
            ft.AddToBackStack(null);
            ft.Commit();
            FragmentManager.ExecutePendingTransactions();
            _adapter = ((MovieFragment)fragment).ListAdapter as MovieAdapter; //((ListFragment)fragment).ListAdapter as ArrayAdapter;
            SupportActionBar.SetTitle(Resource.String.searchAppTitle);

            
        }

      /*  protected override void LoadDB()
        {           
        } */

        private async Task<List<Models.Movie>> GetAllMovies(List<SearchMovie> sMovies)
        {
            var movies = new List<Models.Movie>();
            foreach(SearchMovie sm in sMovies)
            {
                var oMovieTask = OmdbWrapper.Wrapper.GetByIdAsync(sm.ImdbId);
                OMovie oMovie = await oMovieTask;
                movies.Add(Models.Movie.Convert(oMovie));
            }
            return movies;
        }

        private async void Search(object s, Android.Support.V7.Widget.SearchView.QueryTextSubmitEventArgs e)
        {
            RunOnUiThread(() => linearProgress.Visibility = ViewStates.Visible);
            var searchListTask = OmdbWrapper.Wrapper.SearchAsync(e.Query);
            var searchList = await searchListTask;
            _adapter.Clear();
            if (searchList != null)
            {
                var trueSearchList = new List<SearchMovie>();
                foreach (OSearchMovie osm in searchList)
                {
                    trueSearchList.Add(SearchMovie.Convert(osm));
                }
                var moviesTask = GetAllMovies(trueSearchList);
                var movies = await moviesTask;
                _adapter.AddAll(movies);
            }
            else
                Toast.MakeText(this, "No results for: " + e.Query, ToastLength.Short).Show();
            

            /* var inm = (InputMethodManager)GetSystemService(InputMethodService);
             inm.HideSoftInputFromWindow(_searchView.ApplicationWindowToken, HideSoftInputFlags.None);
             _searchView.ClearFocus(); */

            e.Handled = true;
            RunOnUiThread(() => linearProgress.Visibility = ViewStates.Gone);
            _adapter.NotifyDataSetChanged();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            base.OnCreateOptionsMenu(menu);

            _searchView.QueryHint = GetString(Resource.String.searchviewIMDBTitle);
            _searchView.QueryTextChange -= FilterSearchView;
            _searchView.QueryTextSubmit += (s, e) =>
            {
                Search(s, e);   
            };

            menu.FindItem(Resource.Id.search).ExpandActionView();
            _searchView.RequestFocus();

            var search = FindViewById(Resource.Id.search);
            search.Visibility = ViewStates.Visible;

            return true;
        }

        protected override void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var itemTitle = e.MenuItem.TitleFormatted.ToString();
            if (DbLists.ContainsKey(itemTitle))
            {
                Intent resultIntent = new Intent(this, typeof(MainActivity));
                Finish();
                // OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
                Bundle b = new Bundle();
                b.PutString("fragment", itemTitle);
                resultIntent.PutExtras(b);
                StartActivity(resultIntent);
                OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
            }
            else { 
                switch (e.MenuItem.ItemId) { 
                    case (Resource.Id.nav_settings):
                        var ft = FragmentManager.BeginTransaction();
                        fragment = new SettingsFragment();
                        ft.Replace(Resource.Id.HomeFrameLayout, fragment, "SETTINGS");
                        ft.Commit();
                        FragmentManager.ExecutePendingTransactions();
                        var search = FindViewById(Resource.Id.search);
                        search.Visibility = ViewStates.Invisible;
                        SupportActionBar.Title = "Seenflix - Settings";
                        drawerLayout.CloseDrawers();
                        break;
                    case (Resource.Id.nav_netSearch):
                        Finish();
                        //   OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
                        StartActivity(new Intent(this,typeof(SearchActivity)));
                        OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
                        break;
                    case (Resource.Id.nav_add):
                        var transaction = FragmentManager.BeginTransaction();
                        var dialog = new AddFragment();
                        dialog.Show(transaction, "AddFragment");
                        break;
                }
            }
        }
        // Close drawer

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            Adapter.NotifyDataSetChanged();
        }

        public override void OnBackPressed()
        {
            SetResult(Result.Ok);
            //Finish();
            base.OnBackPressed();
            
        }

    }
}