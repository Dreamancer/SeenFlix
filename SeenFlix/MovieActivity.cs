using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.Content.Res;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;

namespace SeenFlix
{
    [Activity(Label = "SeenFlix - movie", MainLauncher = false, Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MovieActivity : AppCompatActivity
    {
        private bool changed = false;
        private Models.Movie movie;
        private string listPath;
        private string listName;
        private int movieID;
        private Dictionary<string, string> DbLists = new Dictionary<string, string>();
        private Spinner movieListSpinner;
        private Button addButton;
             
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var names = Intent.GetStringArrayExtra("DbListNames");
            var paths = Intent.GetStringArrayExtra("DbListPaths");
            for (int i = 0; i < names.Length; i++)
                DbLists.Add(names[i], paths[i]);                    

            var ImdbID =  Intent.GetStringExtra("imdb_id");

            SetContentView(Resource.Layout.Movie);

            movieListSpinner = FindViewById<Spinner>(Resource.Id.movieListSpinner);
            movieListSpinner.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, names);           

            if (ImdbID != null) {
                listName = Intent.GetStringExtra("adding");
                if(listName != null) { 
                    listPath = Intent.GetStringExtra("path");
                }
                else
                {
                    listName = movieListSpinner.SelectedItem.ToString();
                    listPath = DbLists[listName];
                }
                var oMovie = OmdbWrapper.Wrapper.GetById(ImdbID);
                movie = Models.Movie.Convert(oMovie);
                this.Title = movie.Title;

          //      SetButton(ImdbID,listPath);                            
            }
            else
            {
                movieID = Intent.GetIntExtra("id",-1);
                listPath = Intent.GetStringExtra("listPath");
                listName = Intent.GetStringExtra("listName");
                movie = DBHandler.GetMovie(movieID, listPath);
                this.Title = movie.Title;

      //          SetButton(ImdbID, listPath);
            }
            movieListSpinner.SetSelection(Array.IndexOf(names, listName));
            addButton = FindViewById<Button>(Resource.Id.addButton);

            movieListSpinner.ItemSelected += OnSpinnerItemSelected;

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);

            SetSupportActionBar(toolbar);
            SupportActionBar.SetWindowTitle(movie.Title + " - SeenFlix");
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            
            ImageView moviePoster = FindViewById<ImageView>(Resource.Id.moviePoster);
            Bitmap poster = null;
            if ((movie.Poster != "N/A") && ((poster = GetImageBitmapFromUrl(movie.Poster)) != null)) {               
                moviePoster.SetImageBitmap(poster);
            }
            else {
                Drawable temp = ResourcesCompat.GetDrawable(Resources, Resource.Drawable.not_available, null);//GetDrawable(Resource.Drawable.not_available);
                moviePoster.SetImageDrawable(temp);   
            }
            var yearView = FindViewById<TextView>(Resource.Id.movieYear);
            yearView.Text = movie.Year;
            var text = yearView.Text;

            var genreTypeView = FindViewById<TextView>(Resource.Id.movieGenre_type);
            genreTypeView.Text = movie.Genre + ", " + movie.Type;

            var plotView = FindViewById<TextView>(Resource.Id.moviePlot);
            plotView.Text = movie.Plot;
            var sdf = movie.TomatoRating;

			var scoreText = FindViewById<TextView>(Resource.Id.movieScoreText);
			scoreText.Text += movie.ImdbRating;

			var dirText = FindViewById<TextView> (Resource.Id.movieDirector);
			dirText.Text = movie.Director;

			var actorsText = FindViewById<TextView> (Resource.Id.movieActors);
			actorsText.Text = movie.Actors;
        }

        private void SetButton(string path)
        {
            addButton.Click-= ButtonDel;
            addButton.Click -= ButtonAdd;
            Models.Movie delMovie = null;
                try
                {
                    delMovie = DBHandler.GetByImdb(movie.ImdbId, path);
                }
                catch (Exception e) { }     

            if (delMovie != null)
            {
                addButton.Text = "Remove from " + listName;
                addButton.Click += ButtonDel;
            }else
            {
                addButton.Text = "Add to " + listName;
                addButton.Click += ButtonAdd;
            }
        }

        private void OnSpinnerItemSelected (object s, EventArgs e)
        {
            listName = (s as AppCompatSpinner).SelectedItem.ToString();
            listPath = DbLists[listName];
            SetButton(listPath);
        //    movieListSpinner.ItemSelected -= OnSpinnerItemSelected;
        }

        private void ButtonAdd(object s, EventArgs e)
        {
            DBHandler.insertUpdateMovie(movie, listPath);
            changed = !changed;
            Toast.MakeText(this, movie.Title + " added to " + listName, ToastLength.Short).Show();
            var button = s as Button;
            button.Text = "Remove from " + listName;
            button.Click -= ButtonAdd;
            button.Click += ButtonDel;
        }

        private void ButtonDel(object s, EventArgs e)
        {
            DBHandler.Delete(movie, listPath);
            changed = !changed;
            Toast.MakeText(this, movie.Title + " removed from " + listName, ToastLength.Short).Show();
            var button = s as Button;
            button.Text = "Add to " + listName;
            button.Click -= ButtonDel;
            button.Click += ButtonAdd;
        }

            public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)//getItemId())
            {
                case (Android.Resource.Id.Home):
                    if (changed)
                        SetResult(Result.Ok);
                    Finish();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);

            }
        }
        public static Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;
            using (var webClient = new WebClient())
            {
                
                try { 
                    var imageBytes = webClient.DownloadData(url);
                  //  if (imageBytes != null && imageBytes.Length > 0)
                    {
                        imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }
                }
                catch (WebException)
                {
                   //something? 
                }
            }
            return imageBitmap;
        }

        public override void OnBackPressed()
        {
            if (changed)
                SetResult(Result.Ok);
            Finish();
        }
    }
}